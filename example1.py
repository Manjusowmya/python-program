# x=[10,20,30,40,50,60]
# print(x[3])
# x[1]=220
# print(x)
# print(x[:10])
# x=list(range(10,100,5))
# print(x)
# x={
#     'i':1,
#     'j':20,
#     'k':30,
# }
# x['i']=100
# print(x)
# x=5
# if x>10:
#     print('hello')
#     print('manju')
# print('bye')
# x=int(input('enter a number:'))
# print(x)
# y=int(input('enter a 2nd number:'))
# print(y)
#  sum = float(x) + float(y)
#  print('the sum of {0} and{1} is {2}'.format(x,y,sum))
# z=int(input(x-y))
# print(z)

# print('sowmya',end=' ' )
# print('loves',end=' ' )
# print('manju')

# x={10,20,30,40,50,60}
# print(x)

# one=[10,20,30,40]
# two=tuple(one)
# print(two)
# three=set(one)
# print(three)
# four=list(one)
# print(four)
# mytuple=(1,2,3,4,4,3)
# s=set(mytuple)
# print(s)
# string='palle'
# s=tuple(string)
# print(s)

# x=(10,20,30,40,50,60,70,80,90)
# y=set(x)
# print(y)
# z=tuple(x)
# print(z)

# x=10
# if x>15:
#     print('manju')
# else:
#     print('bye')
# print('sowmya')

# using if elif else conditions
# x=50
# if x>1:
#     print('manju')
# if x>20:
#     print('madhu')
# elif x==30:
#     print('amma')
# elif x==50:
#     print('sowmya')
# else:
#     print('bye')

# using nested if
# x=10
# if x>5:
#     print('welcome')
#     if x==5:
#         print('python')
#     else:
#         print('pycharm')
# else:
#     print('bye')

# if with characters

# x=input('enter a chracter:')
# if x=='a':
#  print('a for anu')
# elif x=='b':
#     print('b for ball')
# else:
#     print('cat')

# if with strings

# a=input('enter person name:')
# if a=='sowmya':
#  print(a,'is a student')
# elif a=='manju':
#     print(a,'is a employee')
# else:
#     print(a,'may be trainer')

# while loop sample

# x=1
# while x<=10:
#     print(x)
#     x=x+1
# print('end loop')

# a=[10,20,30,40,50]
# for i in a:
#     print(i)

# using while loop and for loop samples

# x=1
# while x<=100:
#     print(x,end=' ')
#     x=x+2

# for x in range(2,101):
#     print(x,end=" ")
#     x=x+2

# dictionary with loops

# my_dictionary= {
#           'i':10,
#           'j':20
# }
# for x in my_dictionary:
#     print(x,my_dictionary[x])

# for i in range(10):
#     pass

# 1: pass 2 values to a method, and that method has to return sum.

# def function(x,y):
#     return x+y
# sum=function(10,20)
# print(sum)

# 2: pass 1 value to a method, and it should return if it is even or odd.

# positional parameter

# def person_name(first_name,second_name):
#     print(first_name+second_name)
# person_name('manju','sowmya')


# def person_name(first_name, second_name):
#     print(first_name + second_name)
#
#
# # First name is Ram placed first
# # Second name is Babu place second
# person_name("Ram", "Babu")

# def function(x,y):
#     print('x=',x)
#     print('y=',y)
# function(10,20)

# def my_func(name,age):
#     print('name=',name)
#     print('age=',age)
# my_func(name='manju',age=30)

# keyword based parameters

# def func(x,y):
#     print('x=',x)
#     print('y=',y)
# func(y=20,x=10)

# def fun(name,age,locality):
#     print('name=',name)
#     print('age=',age)
#     print('locality=',locality)
# fun(locality='bangalore',name='manju',age=30)

# default parameters sample

# def fun(name,mobile,course='python'):
#     print(name,mobile,course)
# fun('manju',8985977725)

# keyword arbitry parameters sample

# def func(**kwargs):
#     for x,y in kwargs.items():
#         print(x,'=',y)
# func(id=1,name='manju',sub='maths',location='bangalore',course='python')

# arbitary parameters sample

# def func(x,*y,z=100):
#     print(x)
#     print(y)
#     print(z)
# func(10,20,30,40,50,z=60)
# func(50,60,70,80)
# func(8,9,7,4,5,6)

# list methods
# x=[10,20,30,40]
# x.append(55)
# print(x)

# list function append sample

# x=['sowmya','manju','madhu']
# x.append('sanni')
# print(x)

# list function count sample

# x=[1,1,1,2,2,2,2,2,2,3,4,5,6]
# y=x.count(1)
# print(y)

# list function pop sample

# x=[10,20,30,40,50,90,100]
# print(x.pop(7))

# list1 = [ 1, 2, 3, 4, 5, 6 ]
# print(list1.pop())
# print("New List after pop : ", list1, "\n")
# list2 = [1, 2, 3, ('cat', 'bat'), 4]
# print(list2.pop())
# print(list2.pop())
# print(list2.pop())
# print("New List after pop : ", list2, "\n")

# list comprehension

# x=list(range(0,101,2))
# print(x)

# x=[]
# for i in range(0,101,2):
#     x.append(i)
#     print(x)

# x=[i for i in range(0,101,2)]
# print(x)

# x=[i**2 for i in range(1,10)]
# print(x)

# x=[10,20,30,40]
# y=[x[i]*5 for i in range(4)]
# print(y)

# x=[10,20,30,40,50,60,70,80,90,100]
# y=[x[i] for i in range (10) if i % 2 == 1]
# print(y)

# x = [ x for x in range(22) if x % 3 == 0]
# print(x)

# x=[10,20,30,40,50,60]
# y=[]
# for i in range(6):
#       if i%2==1:
#          y.append(x[i])
# print(y)

# Globals

# x=10
# def f1():
#     x=20
#     print(x)
#     print(globals()['x'])
# f1()

# x=10
# y=20
# def f1():
#     y=50
#     print(x)
#     print(y)
#     print(globals()['x'])
#     print(globals()['y'])
# f1()


# x=[10,20,30,40,50,60,70,80,90,100,110,120,130]
# del x[5:8]
# print(x)

# calling method

# class Student:
#     def read(self0):
#         print('subjects')
#     def write(self):
#         print('exams')
# studentone = Student()
# studentone.read()
# studentone.write()

# use of creating objects

# def function():
#     print('welcome')
# function()
# class Doctor:
#     def do_surgery(self):
#         print('doing surgery')
# doctorone = Doctor()
# doctorone.do_surgery()

# init method with parameters

class Pythondeveloper()






























































































































































































































































































































































































































































































































































































































































































































































































